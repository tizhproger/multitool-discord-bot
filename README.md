# Multitool Discord bot



## Getting started

To run a bot use **dsbot.py** file. Don't forget to install all necessary libs for it from requirements.txt.

**Quiz** module is mentioned in main file, however it is not completed yet. It will not work properly.
**Twitch** module was tested and running as planned. However, it requires more testing and checking.
**Music** module for now is a collection of code blocks which must be connected together and to main file. It will not work.

## Files fescription

**dsbot.py** - Main code file which runs the bot and connects all modules together.

**Music** - Module which adds to a bot functionality to listen to music in voice channels. Separate components were tested and they work. However since it was rewritten to be a module, all code pieces should be put together, so for now it is not usable.

**Twitch** - This module contains code related to twitch. For now it implements only one functionality of automatic clips posting from twitch channel, as they appear. In the future it can be extended with other features, but for now it is as it is.

**Quiz** - This module implements a small quiz for obtaining a special role. It plays with one user at a time. This module initially was a part of a main code, but after sometime it was decided to move it to separate module. For now it is not working and should be edited a bit. 

All the text messages and commands descriptions are written in russian. In the future it will be changed to multilanguage, but for now there is only one option.


## Project status
Bot is still in development. Someday it will be finished. If you want to contribute make a request and I will check the changes for the code. If you have problems with it, or you need more explanations - contact me via email or telegram.

**Email**: tizhproger.development@gmail.com

**Telegram**: @tizhprogerr
