import discord
import sqlite3
import os
import configparser
from discord.ext import commands
from discord import VoiceChannel, TextChannel, Role, Interaction, Attachment, Game

if os.path.exists('twitch.py'):
    from twitch import Twitch

if os.path.exists('quiz.py'):
    from quiz import Quiz

# Set your bot's token and command prefix
TOKEN = "TOKEN"
PREFIX = "/"


# Initialize the bot with a command prefix
bot = commands.Bot(command_prefix=PREFIX, intents=discord.Intents.all(), activity=Game(name='настолку'))

allowed_commands = ['!квест']

config = configparser.ConfigParser()

conn = sqlite3.connect('channels_data.db')
cursor = conn.cursor()

# Database section, creation of tables etc.
# Create a table to store voice channel-role associations if it doesn't exist

cursor.execute('''
    CREATE TABLE IF NOT EXISTS channel_roles (
        channel_name TEXT PRIMARY KEY,
        role_name TEXT
    )
''')

cursor.execute('''
    CREATE TABLE IF NOT EXISTS channel_transfer (
        source_channel TEXT PRIMARY KEY,
        target_channel TEXT
    )
''')

cursor.execute('''
    CREATE TABLE IF NOT EXISTS command_channel (
        id SERIAL PRIMARY KEY,
        guild_id INTEGER,
        channel_id INTEGER
    )
''')

cursor.execute('''
    CREATE TABLE IF NOT EXISTS transfer_message (
        message_id INTEGER PRIMARY KEY,
        target_channel TEXT,
        source_channel TEXT,
        UNIQUE(target_channel, source_channel)
    )
''')

conn.commit()


async def check_config(config, section=None):
    config.read('config.ini')
    uninitialized_variables = []
    sections = config.sections() if section is None else [section]
    for section in sections:
        for key, value in config.items(section):
            if value is None or value.strip() == '':
                uninitialized_variables.append(f"{section}.{key}") 
    if len(uninitialized_variables) > 0:
        return uninitialized_variables
    else:
        return 'OK'

# Event handler for when the bot is ready
### Rework quiz and twitch functions to work properly with interactions ###
@bot.event
async def on_ready():
    global quiz, clips
    print(f"We have logged in as {bot.user.name}")

    if not os.path.exists('./config.ini'):
        config['TWITCH'] = {'clips_channel': '', 
                            'twitch_channel': '', 
                            'client_id': '',
                            'client_secret': '',
                            'twitch_token': '',
                            'refresh_token': ''}
        
        config['QUIZ'] = {'quiz_role': '',
                          'prize_role': '',
                          'quiz_channel': '',
                          'secret_voice': '',
                          'secret_text': '',
                          'greetings_folder': ''} 

        with open('config.ini', 'w') as configfile:
            config.write(configfile)
    else:
        config.read('config.ini')

    res = await check_config(config, 'QUIZ')
    if res == 'OK':
        quiz = Quiz(quiz_role=config['QUIZ']['quiz_role'],
            prize_role=config['QUIZ']['prize_role'],
            quiz_channel=config['QUIZ']['quiz_channel'],
            secret_voice=config['QUIZ']['secret_voice'],
            secret_text=config['QUIZ']['secret_text'])

        @bot.tree.command(name="квест", description="Start a quiz and unlock the chamber of secrets")
        async def start_quiz(interaction: Interaction):
            await quiz.start_quiz(interaction)
    
    res = await check_config(config, 'TWITCH')
    if res == 'OK':
        clips = Twitch(clips_channel=config['TWITCH']['clips_channel'], 
            twitch_channel=config['TWITCH']['twitch_channel'], 
            client_id=config['TWITCH']['client_id'],
            client_secret=config['TWITCH']['client_secret'],
            twitch_token=config['TWITCH']['twitch_token'],
            refresh_token=config['TWITCH']['refresh_token'])

        @bot.tree.command(name="clipson", description="Включить автопостинг клипов твича")
        async def start_clips(interaction: Interaction):
            if not clips.check_for_clips.is_running():
                await interaction.response.send_message('Мониторинг клюпов твича включен...')
                
                await clips.check_for_clips.start(interaction)
            else:
                await interaction.response.send_message('Мониторинг клюпов твича уже включен...')
        
        @bot.tree.command(name="clipsoff", description="Выключить автопостинг клипов твича")
        async def stop_clips(interaction: Interaction):
            if clips.check_for_clips.is_running():
                await interaction.response.send_message('Мониторинг клюпов твича выключен...')
                
                clips.check_for_clips.stop()
            else:
                await interaction.response.send_message('Мониторинг клюпов твича уже выключен...')


@bot.event
async def on_raw_reaction_add(payload):
    if payload.member.bot:
        return
    
    cursor.execute('SELECT target_channel, source_channel FROM transfer_message WHERE message_id = ?', (payload.message_id,))
    channels = cursor.fetchone()
    if channels is not None:
        target, source = channels[0], channels[1]
        user = payload.member
        msg = await bot.get_channel(payload.channel_id).fetch_message(payload.message_id)
        guild = bot.get_guild(payload.guild_id)
        
        if user.guild_permissions.administrator:
            if payload.emoji.name == '🔓':
                await msg.remove_reaction(payload.emoji, payload.member)
                result = await reveal_channel(guild, source)
                if result is None:
                    await msg.channel.send('Трансфер не найден, удаляю сообщение...', delete_after=3)
                    await msg.delete()
                
            elif payload.emoji.name == '🔐':
                await msg.remove_reaction(payload.emoji, payload.member)
                result = await hide_channel(guild, source)
                if result is None:
                    await msg.channel.send('Трансфер не найден, удаляю сообщение...', delete_after=3)
                    await msg.delete()
            
            elif payload.emoji.name == '🧹':
                await msg.remove_reaction(payload.emoji, payload.member)
                result = await clear_channel(guild, source)
                if result is None:
                    await msg.channel.send('Трансфер не найден, удаляю сообщение...', delete_after=3)
                    await msg.delete()
            
            elif payload.emoji.name == '❌':
                await msg.remove_reaction(payload.emoji, payload.member)
                channel = bot.get_channel(payload.channel_id)
                result = await remove_transfer(guild, channel, source)
                if result is None:
                    await msg.channel.send('Трансфер не найден, удаляю сообщение...', delete_after=3)
                    await msg.delete()
    else:
        msg = await bot.get_channel(payload.channel_id).fetch_message(payload.message_id)
        await msg.channel.send('Трансфер не найден, удаляю сообщение...', delete_after=3)
        await msg.delete()

# Event handler for when user joins voice channel, check if transfer is set for it
# If yes - moves user another voice channel
@bot.event
async def on_voice_state_update(member, before, after):
    try:
        if before.channel is None and after.channel is not None:
            channel_name = after.channel.name
            channel = after.channel
        elif before.channel is not None and after.channel is None:
            channel_name = before.channel.name
            channel = before.channel
        elif before.channel != after.channel:
            channel_name = before.channel.name
            channel = before.channel
            
        cursor.execute('SELECT role_name FROM channel_roles WHERE channel_name = ?', (channel_name,))
        role_name = cursor.fetchone()
        if role_name:
            role = discord.utils.get(member.guild.roles, name=role_name[0])
            if before.channel != after.channel:
                if after.channel:
                    await member.add_roles(role)
                    return
                elif before.channel:
                    await member.remove_roles(role)
                    return

        cursor.execute('SELECT target_channel FROM channel_transfer WHERE source_channel = ?', (channel_name,))
        target_channel = cursor.fetchone()
        if target_channel is not None and channel == member.voice.channel:
            guild = member.guild
            target_vc = discord.utils.get(guild.voice_channels, name=target_channel[0])
            if target_vc:
                await member.move_to(target_vc)
                return
                
    except Exception as e:
        print('Line error: ' + str(e.__traceback__.tb_lineno) + ', Message: "' + str(e) + '"')

async def check_channel(interaction: Interaction):
    cursor.execute('SELECT * FROM command_channel')
    check_res = cursor.fetchall()

    if len(check_res) > 0:
        cursor.execute('SELECT channel_id FROM command_channel WHERE guild_id = ?', (interaction.guild_id,))
        command_channel_id = [id[0] for id in cursor.fetchall()]

        if command_channel_id is None:
            await interaction.response.send_message('Для этого сервера не установлен управляющий канал бота', ephemeral=True)
            return False

        if interaction.channel_id not in command_channel_id:
            await interaction.response.send_message('Мне запрещено работать тут', ephemeral=True)
            return False
        
        return True

    else:
        return True


####################################################################
        
@bot.command(name='sync', description='Синхронизировать команды бота')
async def sync(ctx):
    print("Sync commands tree...")
    if ctx.message.author.id == 512781943700455424:
        await bot.tree.sync()
        await ctx.channel.send('Command tree synced.')
        await ctx.message.delete()
    else:
        await ctx.channel.send('You must be the owner to use this command!')
        await ctx.message.delete()

# Section for setting, listing and removing transfer between channels
# Command to set transfer from one channel to another, like !transfer {from_channel_name} {to_channel_name}
@bot.tree.command(name='transfer', description='Установить перенос из одного войса в другой')
async def set_transfer(interaction: Interaction, from_channel: VoiceChannel, to_channel: VoiceChannel):
    if interaction.user.guild_permissions.administrator:
        if not await check_channel(interaction):
            return
        
        if from_channel is not None and to_channel is not None:
            cursor.execute('INSERT OR REPLACE INTO channel_transfer (source_channel, target_channel) VALUES (?, ?)', (from_channel.name, to_channel.name))
            conn.commit()

            await send_control_message(interaction, from_channel, to_channel)
        else:
            await interaction.response.send_message('Один или несколько указанных каналов не существет.', ephemeral=True)
    else:
        await interaction.response.send_message("У вас нет прав для этой команды", ephemeral=True)


@bot.tree.command(name='untransfer', description='Удалить перенос из одного войса в другой')
async def del_transfer(interaction: Interaction, channel: VoiceChannel):
    if interaction.user.guild_permissions.administrator:
        if not await check_channel(interaction):
            return
        
        cursor.execute('SELECT * FROM channel_transfer WHERE source_channel = ?', (channel.name,))
        res = cursor.fetchone()

        if res is None:
            await interaction.response.send_message(f'Не найден трансфер дла канала: <#{channel.id}>')
        else:
            cursor.execute('DELETE FROM channel_transfer WHERE source_channel=?', (channel.name,))
            conn.commit()

            await interaction.response.send_message(f"Трансфер удален для канала: <#{channel.id}>")
    else:
        await interaction.response.send_message("У вас нет прав для этой команды", ephemeral=True)
        

# Command to view all saved transfers
@bot.tree.command(name='transfers', description='Показать все сохраненные переносы')
async def list_transfers(interaction: Interaction):
    if interaction.user.guild_permissions.administrator:
        if not await check_channel(interaction):
            return
        
        cursor.execute('SELECT * FROM channel_transfer')
        transfers = cursor.fetchall()
        if transfers:
            transfers_str = ''
            for source_channel, target_channel in transfers:
                source = discord.utils.get(interaction.guild.voice_channels, name=source_channel)
                target = discord.utils.get(interaction.guild.voice_channels, name=target_channel)
                transfers_str += f'<#{source.id}> -> <#{target.id}>\n'

            await interaction.response.send_message('Текущие трансферы:\n' + transfers_str, ephemeral=True)
        else:
            await interaction.response.send_message('Трансферы не найдены.', ephemeral=True)
    else:
        await interaction.response.send_message("У вас нет прав для этой команды", ephemeral=True)
        


####################################################################
# Section for sending control messages for transfer feature
# Function to send control message with emojis for channel with association
async def send_control_message(interaction, source_vc, target_vc):
    await interaction.response.send_message(f"Трансфер между каналами:\n<#{source_vc.id}> -> <#{target_vc.id}>")
    service_message = await interaction.original_response()
    cursor.execute('INSERT OR REPLACE INTO transfer_message (message_id, source_channel, target_channel) VALUES (?, ?, ?)', (service_message.id, source_vc.name, target_vc.name))
    conn.commit()

    await service_message.add_reaction('🔓')  # Open emoji
    await service_message.add_reaction('🔐')  # Close emoji
    await service_message.add_reaction('🧹')  # Clean emoji
    await service_message.add_reaction('❌')  # Remove transfer

async def delete_control_message(guild, channel, source_channel):
    cursor.execute('SELECT channel_id FROM command_channel WHERE guild_id = ?', (guild.id,))
    channels = cursor.fetchone()
    if channels is not None:
        channel = bot.get_channel(channels[0])

    cursor.execute(f'SELECT message_id FROM transfer_message WHERE source_channel = ?', (source_channel,))
    transfer_message = cursor.fetchone()[0]
    msg = await channel.fetch_message(transfer_message)
    await msg.delete()

    cursor.execute(f'DELETE FROM transfer_message WHERE message_id = ?', (transfer_message,))
    conn.commit()

async def reveal_channel(guild, source_channel):
    source_vc = discord.utils.get(guild.voice_channels, name=source_channel)
    if source_vc is not None:
        await source_vc.set_permissions(guild.default_role, view_channel=True)
        return True
    else:
        return None

async def hide_channel(guild, source_channel):
    source_vc = discord.utils.get(guild.voice_channels, name=source_channel)
    if source_vc is not None:
        await source_vc.set_permissions(guild.default_role, view_channel=False)
        return True
    else:
        return None

async def clear_channel(guild, source_channel):
    source_vc = discord.utils.get(guild.voice_channels, name=source_channel)
    if source_vc is not None:
        cursor.execute('SELECT target_channel FROM channel_transfer WHERE source_channel = ?', (source_channel,))
        target_channel = cursor.fetchone()
        if target_channel is not None:
            target_vc = discord.utils.get(guild.voice_channels, name=target_channel[0])
            if target_vc is not None:
                for member in target_vc.members:
                    await member.move_to(None)
        return True
    else:
        return None

async def remove_transfer(guild, channel, source_channel):
    source_vc = discord.utils.get(guild.voice_channels, name=source_channel)
    if source_vc is not None:
        cursor.execute('DELETE FROM channel_transfer WHERE source_channel = ?', (source_channel,))
        conn.commit()
        await delete_control_message(guild, channel, source_channel)
        return True
    else:
        return False


####################################################################
# Section for setting associations between voice channels and roles for them, roles auto assignment
# Command to set association for channel with role, like !bind {channel_name} {role_name}
@bot.tree.command(name='bind', description='Привязать роль для войса')
async def add_association(interaction: Interaction, channel: VoiceChannel, role: Role):
    if interaction.user.guild_permissions.administrator:
        if not await check_channel(interaction):
            return
        
        channel = bot.get_channel(channel.id)
        role = discord.utils.get(interaction.guild.roles, id=role.id)

        if role is not None:
            cursor.execute('INSERT OR REPLACE INTO channel_roles (channel_name, role_name) VALUES (?, ?)', (channel.name, role.name))
            conn.commit()
            await interaction.response.send_message(f'Связка добавлена\nГолосовой канал: <#{channel.id}>\nРоль: <@&{role.id}>')
        else:
            await interaction.response.send_message(f'Роль не найдена на сервере: {role.name}', ephemeral=True)       
    else:
        await interaction.response.send_message("У вас нет прав для этой команды", ephemeral=True)
        

# Command to remove association from channel, like !unbind {channel_name}
@bot.tree.command(name='unbind', description='Отвязать роль от войса')
async def remove_association(interaction: Interaction, channel: VoiceChannel):
    if interaction.user.guild_permissions.administrator:
        if not await check_channel(interaction):
            return
        
        cursor.execute('SELECT * FROM channel_roles WHERE channel_name = ?', (channel.name,))
        res = cursor.fetchone()

        if res is None:
            await interaction.response.send_message(f'Не найдена связка дла голосового канала: <#{channel.id}>')
        else:
            cursor.execute('DELETE FROM channel_roles WHERE channel_name = ?', (channel.name,))
            conn.commit()
            await interaction.response.send_message(f'Связка убрана для голосового канала: <#{channel.id}>')
    else:
        await interaction.response.send_message("У вас нет прав для этой команды", ephemeral=True)
        

# Command to view all roles and voice channels associations
@bot.tree.command(name='binds', description='Показать все связки')
async def list_associations(interaction: Interaction):
    if interaction.user.guild_permissions.administrator:
        if not await check_channel(interaction):
            return
        
        cursor.execute('SELECT * FROM channel_roles')
        associations = cursor.fetchall()
        associations_str = ""
        if associations:
            for channel_name, role_name in associations:
                role = discord.utils.get(interaction.guild.roles, name=role_name)
                channel = discord.utils.get(interaction.guild.voice_channels, name=channel_name)
                associations_str += f'<#{channel.id}> -> <@&{role.id}>\n'

            await interaction.response.send_message('Текущие связки:\n' + associations_str, ephemeral=True)
        else:
            await interaction.response.send_message('Связки не найдены.', ephemeral=True)
    else:
        await interaction.response.send_message("У вас нет прав для этой команды", ephemeral=True)


####################################################################
# Section with other commands
# Command to send a message to a specific channel as a bot, like !send {channel_name} {message}
@bot.tree.command(name="send", description='Отправить сообщение в канал от имени бота')
async def send_message(interaction: Interaction, channel: TextChannel, message_text: str):
    if interaction.user.guild_permissions.administrator:
        if not await check_channel(interaction):
            return
        
        if channel:
            await channel.send(message_text)
            await interaction.response.send_message(f'Сообщение отправлено в <#{channel.id}>', ephemeral=True)
        else:
            await interaction.response.send_message(f"Канал '{channel.name}' не найден.", ephemeral=True)
    else:
        await interaction.response.send_message("У вас нет прав для этой команды", ephemeral=True)
        

# Command to get channels as control channels for bot. Only commands here will be heared by bot.
@bot.tree.command(name='getchannels', description='Посмотреть управляющие каналы')
async def get_command_channel(interaction: Interaction):
    if interaction.user.guild_permissions.administrator:
        if not await check_channel(interaction):
            return
        
        cursor.execute('SELECT channel_id FROM command_channel')
        channels = cursor.fetchall()
        channels_str = ""
        if channels:
            for channel_id in channels:
                channels_str += f'<#{channel_id[0]}>\n'

            await interaction.response.send_message('Текущие командные каналы:\n' + channels_str, ephemeral=True)
        else:
            await interaction.response.send_message('Каналы не найдены.', ephemeral=True)
    else:
        await interaction.response.send_message("У вас нет прав для этой команды", ephemeral=True)


# Command to set channel as control channel for bot. Only commands here will be heared by bot.
@bot.tree.command(name='setchannel', description='Установить канал в качестве управляющего')
async def set_command_channel(interaction: Interaction, channel: TextChannel):
    if interaction.user.guild_permissions.administrator:
        if not await check_channel(interaction):
            return
        
        guild_id = interaction.guild.id
        cursor.execute(f"INSERT OR IGNORE INTO command_channel (guild_id, channel_id) VALUES (?, ?)", (guild_id, channel.id))
        conn.commit()

        await interaction.response.send_message(f'Канал <#{channel.id}> теперь привязан к боту.', ephemeral=True)
    else:
        await interaction.response.send_message("У вас нет прав для этой команды", ephemeral=True)
        

# Command to remove channel as control channel for bot.
@bot.tree.command(name='remchannel', description='Отвязать сохраненный управляющий канал')
async def remove_command_channel(interaction: Interaction, channel: TextChannel):
    if interaction.user.guild_permissions.administrator:
        if not await check_channel(interaction):
            return
        
        guild_id = interaction.guild.id
        cursor.execute('DELETE FROM command_channel WHERE guild_id = ? AND channel_id = ?', (guild_id, channel.id))
        conn.commit()

        await interaction.response.send_message(f'Канал <#{channel.id}> больше не привязан к боту.', ephemeral=True)
    else:
        await interaction.response.send_message("У вас нет прав для этой команды", ephemeral=True)
        

@bot.tree.command(name='checkconf', description='Проверить конфиг бота')
async def config_check(interaction: Interaction):
    if interaction.user.guild_permissions.administrator:
        if not await check_channel(interaction):
            return
        
        res = await check_config(config)
        if res == 'OK':
            await interaction.response.send_message('Состояние конфига - OK', ephemeral=True)
        else:
            await interaction.response.send_message('Вы не указали в конфиге:\n' + '\n'.join(res), ephemeral=True)
    else:
        await interaction.response.send_message("У вас нет прав для этой команды", ephemeral=True)
        

@bot.tree.command(name='getconfig', description='Выгрузить файл конфига')
async def get_config(interaction: Interaction):
    if interaction.user.guild_permissions.administrator:
        if not await check_channel(interaction):
            return
        
        await interaction.response.send_message("Вот конфиг:", file=discord.File("config.ini"), ephemeral=True)
    else:
        await interaction.response.send_message("У вас нет прав для этой команды", ephemeral=True)
        

@bot.tree.command(name='setconfig', description='Загрузить конфиг из файла')
async def load_config(interaction: Interaction, file: Attachment):
    if interaction.user.guild_permissions.administrator:
        if not await check_channel(interaction):
            return
        
        if file.filename.endswith('.ini'):
            file_content = await file.read()

            with open('config.ini', 'wb') as file:
                file.write(file_content)

            await interaction.response.send_message("Конфиг успешно загружен", ephemeral=True)
        else:
            await interaction.response.send_message("Ошибка: Прикрепленный файл должен иметь расширение .ini", ephemeral=True)
    else:
        await interaction.response.send_message("У вас нет прав для этой команды", ephemeral=True)
        

bot.run(TOKEN)
