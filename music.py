import discord
from discord.ext import commands, tasks
from datetime import datetime
from yt_dlp import YoutubeDL
import asyncio

class Music(commands.Bot):

    def __init__(self, message_delete_delay, bot):
        self.queue = {}
        self.last_play_time = {}
        self.message_delete_delay = 15
        self.bot = ''

    # Function to start playing qeued music
    async def play_queue(self, ctx):
        voice_client = self.bot.voice_clients
        message = ''
        channel = False if ctx.author.voice is None else ctx.author.voice.channel

        if not channel:
            await ctx.send("You are not connected to a voice channel.")
            return

        if ctx.guild.id in self.queue and self.queue[ctx.guild.id]:
            if len(voice_client) == 0:
                voice_channel = await channel.connect()
            else:
                voice_channel = voice_client[0]

            while self.queue[ctx.guild.id]:
                track = self.queue[ctx.guild.id][0]
                del self.queue[ctx.guild.id][0]

                try:
                    with YoutubeDL({'format': 'bestaudio', 'postprocessors': [{
                                    'key': 'FFmpegExtractAudio',
                                    'preferredcodec': 'mp3',
                                    'preferredquality': '320', }]}) as ydl:
                        
                        info = ydl.extract_info(track['url'], download=False)
                        url2 = info['url']
                        voice_channel.play(discord.FFmpegPCMAudio(source=url2, before_options="-reconnect 1 -reconnect_streamed 1 -reconnect_delay_max 5"), after=lambda e: print('done', e))
                        voice_channel.source = discord.PCMVolumeTransformer(voice_channel.source)
                        voice_channel.source.volume = 0.5

                    if message == '':
                        message = await ctx.send(f"Now playing: {track['name']}")
                    else:
                        await message.edit(content=f"Now playing: {track['name']}")

                    while voice_channel.is_playing():
                        await asyncio.sleep(1)

                except Exception as e:
                    await ctx.send(f'Error playing the track: {str(e)}')
                finally:
                    self.last_play_time[ctx.guild.id] = datetime.now()  # Update the last play time

        else:
            await ctx.send('Queue is empty.')
    
    @tasks.loop(seconds=20)  # Check every 60 seconds
    async def check_idle_channels(self):
        for guild in self.bot.guilds:
            if guild.id in self.last_play_time:
                # Check if the bot is in a voice channel and has been idle for more than 2 minutes
                if self.bot.voice_clients and self.bot.voice_clients[0].guild == guild:
                    time_since_last_play = datetime.now() - self.last_play_time[guild.id]
                    if time_since_last_play.total_seconds() > 120:
                        await self.bot.voice_clients[0].disconnect()
                        print(f"Disconnected from idle voice channel in {guild.name}")
    
    ####################################################################
    # Section for playing, stopping, clearing and pausing music on playing
    # Command to play the song by url
    @bot.command(name='play', help='Play a song from the specified link and add it to the queue. Resume playing if no URL is specified.')
    async def play(self, ctx, url=None):
        global is_command_running
        if is_command_running:
            await ctx.send('The bot is currently busy processing another command. Please wait.', delete_after=self.message_delete_delay)
            return
        
        try:
            voice_client = self.bot.voice_clients
            channel = False if ctx.author.voice is None else ctx.author.voice.channel
            if not channel:
                await ctx.send("You are not connected to a voice channel.", delete_after=self.message_delete_delay)
                return
            else:
                if len(voice_client) == 0:
                    voice_channel = await channel.connect()
                else:
                    voice_channel = voice_client[0]

            if ctx.guild.id not in self.queue:
                self.queue[ctx.guild.id] = []
            
            # Check if the bot is already connected to a voice channel and playing
            if self.bot.voice_clients and self.bot.voice_clients[0].is_playing():
                await ctx.send('The bot is currently playing in another voice channel. Please wait until it finishes.', delete_after=self.message_delete_delay)
                is_command_running = False  # Reset the flag
                return
            
            if url:
                try:
                    with YoutubeDL({'format': 'bestaudio', 'noplaylist': True, 'ignoreerrors': True}) as ydl:
                        info = ydl.extract_info(url, download=False)

                        if info['webpage_url'] not in self.queue[ctx.guild.id]:
                            track_info = f'**{info["title"]} by {info["artist"]}**' if "artist" in info else f'**{info["title"]}**'
                            self.queue[ctx.guild.id].append({'url': info['webpage_url'], 'name': track_info})
                            await ctx.send(f'{track_info} *added to the queue*.')
                        else:
                            await ctx.send('The specified track is already in the queue.', delete_after=self.message_delete_delay)

                    if not voice_channel.is_playing():
                        await self.play_queue(ctx)

                except Exception as e:
                    await ctx.send(f'Error processing the link, sorry', delete_after=self.message_delete_delay)
            
            elif self.bot.voice_clients:
                if self.bot.voice_clients[0].is_paused():
                    self.bot.voice_clients[0].resume()
                    message = ctx.message
                    await message.delete()

                elif not voice_channel.is_playing():
                    await self.play_queue(ctx)
                    message = ctx.message
                    await message.delete()
            else:
                await ctx.send('No URL specified, and I am not currently playing.', delete_after=self.message_delete_delay)
        except Exception as e:
            print(e)

    # Command to add whole playlist to the queue
    @bot.command(name='playlist', help='Add all tracks from the specified playlist link to the queue.')
    async def playlist(self, ctx, url=None):
        global is_command_running
        if is_command_running:
            await ctx.send('The bot is currently busy processing another command. Please wait.', delete_after=self.message_delete_delay)
            return

        is_command_running = True

        channel = False if ctx.author.voice is None else ctx.author.voice.channel
        if not channel:
            await ctx.send("You are not connected to a voice channel.", delete_after=self.message_delete_delay)
            return

        if ctx.guild.id not in self.queue:
            self.queue[ctx.guild.id] = []
        
        # Check if the bot is already connected to a voice channel and playing
        if self.bot.voice_clients and self.bot.voice_clients[0].is_playing():
            await ctx.send('The bot is currently playing in another voice channel. Please wait until it finishes.', delete_after=self.message_delete_delay)
            is_command_running = False  # Reset the flag
            return

        if url:
            message = await ctx.send(f"Extracting data from playlist...")
            try:
                with YoutubeDL({'format': 'bestaudio', 'ignoreerrors': True}) as ydl:
                    info = ydl.extract_info(url, download=False)

                    if 'entries' in info:
                        tracks = [x for x in info['entries'] if x is not None]
                        await message.edit(content=f"Adding {len(tracks)} tracks to the queue, wait a bit...")
                        # Playlist link, add all tracks to the queue
                        for track in tracks:
                            track_url = track['url']
                            if track_url not in self.queue[ctx.guild.id]:
                                self.queue[ctx.guild.id].append({'url': track_url, 'name': f'**{track["title"]} by {track["artist"]}**' if "artist" in track else f'**{track["title"]}**'})
                            await message.edit(content=f"Adding track: **({str(tracks.index(track) + 1)}/{str(len(tracks))})**")

                        await message.edit(content=f"Added **{len(tracks)} tracks**")
                        await self.play_queue(ctx)
                        is_command_running = False
                    else:
                        message.edit(content='No entries found in the playlist link.', delete_after=self.message_delete_delay)
            except Exception as e:
                await message.edit(content=f'Error processing the playlist link: {str(e)}', delete_after=self.message_delete_delay)
        else:
            await ctx.send('No URL specified for the playlist.', delete_after=self.message_delete_delay)
        is_command_running = False
            
    # Command to sview the current playing qeue
    @bot.command(name='queue', help='Show the current queue')
    async def show_queue(self, ctx):
        message = ctx.message
        await message.delete()
        if ctx.guild.id in self.queue and self.queue[ctx.guild.id]:
            queue_titles = []
            if len(self.queue[ctx.guild.id]) > 10:
                await ctx.send(f'To many tracks in queue to display: {str(len(self.queue[ctx.guild.id]))}', delete_after=self.message_delete_delay)
            else:
                for track in self.queue[ctx.guild.id][:10]:
                    track_info = f'**{track["name"]}**'
                    queue_titles.append(track_info)

                message = '\n'.join(queue_titles)
                await ctx.send(f"Current queue: \n{message}")
        else:
            await ctx.send('Queue is empty.', delete_after=self.message_delete_delay)

    # Command to stop the playing, clear qeue and disconnect
    @bot.command(name='stop', help='Stop playing, clear the queue, and disconnect from the voice channel')
    async def stop(self, ctx):
        message = ctx.message
        await message.delete()
        if ctx.voice_client:
            ctx.voice_client.stop()
            self.queue[ctx.guild.id] = []
            await ctx.voice_client.disconnect()
            await ctx.send('Stopped playing and cleared the queue.', delete_after=self.message_delete_delay)
        else:
            await ctx.send("I'm not connected to a voice channel.", delete_after=self.message_delete_delay)

    # Command to pause the current playing song
    @bot.command(name='pause', help='Pause playing')
    async def pause(self, ctx):
        message = ctx.message
        await message.delete()
        if ctx.voice_client and ctx.voice_client.is_playing():
            ctx.voice_client.pause()
        else:
            await ctx.send("I'm not playing anything.", delete_after=self.message_delete_delay)

    # Command to clear the qeue
    @bot.command(name='clear', help='Clear the queue and stop playing, but stay in the voice channel')
    async def clear(self, ctx):
        message = ctx.message
        await message.delete()
        if ctx.voice_client:
            ctx.voice_client.stop()
            self.queue[ctx.guild.id] = []
            await ctx.send('Cleared the queue and stopped playing.', delete_after=self.message_delete_delay)
        else:
            await ctx.send("I'm not connected to a voice channel.", delete_after=self.message_delete_delay)

    # Command to skip the current playing song
    @bot.command(name='skip', help='Skip the current track and go to the next one in the queue')
    async def skip(self, ctx):
        message = ctx.message
        await message.delete()
        if ctx.voice_client and ctx.voice_client.is_playing():
            ctx.voice_client.stop()
            if len(self.queue[ctx.guild.id]) == 0:
                await ctx.send('Queue is empty.', delete_after=self.message_delete_delay)
        else:
            await ctx.send("I'm not playing anything.", delete_after=self.message_delete_delay)

    