import discord
from discord.ext import commands
import random
import asyncio
import traceback

class Quiz():

    def __init__(self, quiz_role, prize_role, quiz_channel, secret_voice, secret_text):
        self.ROLE_QUIZ = quiz_role
        self.ROLE_PRIZE = prize_role
        self.CHANNEL_NAME = quiz_channel
        self.SECRET_VOICE = secret_voice
        self.SECRET_TEXT = secret_text
        self.quests = [
            {"question": "Что может быть больше у человека, чем у коровы?", "answer": "Страховка"},
            {"question": "Что можно сломать, даже если не трогать?", "answer": "Обещание"},
            {"question": "Что идет, не идет, а к нам всегда приходит?", "answer": "Завтра"},
            {"question": "Что начинается и заканчивается чем-то, но при этом ничего не меняется?", "answer": "Цифра"},
            {"question": "Что можно увидеть с закрытыми глазами?", "answer": "Сон"},
            {"question": "Какой объект можно увидеть в зеркале, но не потрогать?", "answer": "Рефлексия"},
            {"question": "Что идет вверх, но никогда не спускается?", "answer": "Возраст"},
            {"question": "Что может стать мостом между прошлым и будущим?", "answer": "Настоящее"},
            {"question": "Что можно слышать, но никогда не увидеть?", "answer": "Тайна"},
            {"question": "Что летает без крыльев и падает без парашюта?", "answer": "Время"},
            {"question": "Что у человека есть, у птицы нет, а у рыбы может быть?", "answer": "Фамилия"},
            {"question": "Что у каждого человека только один раз в жизни?", "answer": "Вчера"},
            {"question": "Что можно сломать, словно не держится, если его не сломать?", "answer": "Молчание"},
            {"question": "Что всегда перед вами, но никогда не может быть достигнуто?", "answer": "Будущее"},
            {"question": "Что можно взять, но нельзя подержать?", "answer": "Фотография"},
            {"question": "Что можно увидеть с высоты, но не с земли?", "answer": "Облака"},
            {"question": "Что бегает по лесу на четырех ногах утром, на двух ногах днем и на трех ногах вечером?", "answer": "Человек"},
            {"question": "Что можно слушать, но нельзя сказать?", "answer": "Ответ: Молчание"},
            {"question": "Что летит без крыльев, плачет без глаз, внутри бесконечно, а снаружи - конец?", "answer": "Яйцо"},
            {"question": "Что идет, не оставляя следов?", "answer": "Мысль"},
            {"question": "Что делает человек, стоя на одной ноге, потом на двух и, наконец, на трех?", "answer": "Рост"},
            {"question": "Что у всех есть, у кого-то больше, а у кого-то меньше?", "answer": "Возможности"},
            {"question": "Что имеет ключ, но не открывает замок?", "answer": "Пианино"},
            {"question": "Что всегда перед вами, но никогда не может быть достигнуто?", "answer": "Будущее"},
            {"question": "Что можно видеть в минуту, в секунду, но невозможно увидеть год?", "answer": "Письмо"},
            {"question": "Что можно слышать, но нельзя коснуться?", "answer": "Голос"},
            {"question": "Что начинается и заканчивается с кем-то?", "answer": "Шутка"},
            {"question": "Что проходит через воздух без шума?", "answer": "Ответ: Сон"},
            {"question": "Что можно сломать, пока пытаешься исправить?", "answer": "Сердце"},
            {"question": "Что приходит, когда человек зовет, но никогда не приходит, когда его ждут?", "answer": "Завтра"},
            {"question": "Что всегда у тебя в руке, но никогда не видно?", "answer": "Будущее"},
            {"question": "Что делает человек, стоя на одной ноге, потом на двух и, наконец, на трех?", "answer": "Растет"},
            {"question": "Что можно разбить, даже если оно не падает?", "answer": "Рекорд"},
            {"question": "Что можно увидеть с закрытыми глазами?", "answer": "Сон"},
            {"question": "Что нельзя удержать, даже если сильно держать?", "answer": "Дыхание"},
        ]

    async def notify_prize_winners(self, ctx):
        notification_channel = discord.utils.get(ctx.guild.channels, name=self.SECRET_TEXT)
        role = discord.utils.get(ctx.guild.roles, name=self.ROLE_PRIZE)
        if not notification_channel:
            print(f"Канал для уведомлений '{self.SECRET_TEXT}' не найден.")
            return
        
        notification_message = f"<@&{role.id}>\n🎉 Новый учаснтник прибыл! Поздравляем <@{ctx.author.id}> с прохождением квеста! 🎉"
        await notification_channel.send(notification_message)

    async def start_quiz(self, ctx):
        await ctx.message.delete()

        if ctx.channel.name != self.CHANNEL_NAME:
            return

        if discord.utils.get(ctx.author.roles, name=self.ROLE_QUIZ) is not None or discord.utils.get(ctx.author.roles, name=self.ROLE_PRIZE) is not None:
            return

        if ctx.author.voice and ctx.author.voice.channel:
            voice_channel = ctx.author.voice.channel
        else:
            voice_channel = None

        role_quiz = discord.utils.get(ctx.guild.roles, name=self.ROLE_QUIZ)
        await ctx.author.add_roles(role_quiz)

        await ctx.channel.set_permissions(ctx.guild.default_role, read_messages=False)
        await ctx.channel.set_permissions(role_quiz, read_messages=True)

        quest = random.choice(self.quests)
        question = quest["question"]
        answer = quest["answer"]

        question_message = await ctx.send(f"{question}\n У тебя есть 60 секунд что бы ответить!", delete_after=60)

        def check(m):
            if m.author == ctx.author:
                if m.content.lower() != answer.lower():
                    asyncio.create_task(m.delete())
                return m.content.lower() == answer.lower()

        try:
            user_answer = await ctx.bot.wait_for("message", check=check, timeout=60.0)
            await question_message.delete()
            await ctx.send(f"Конграц, {ctx.author.mention}! Ты прошел квест и получио приз.\nПрисмотрись, может что-то на сервере изменилось...", delete_after=60)

            role_prize = discord.utils.get(ctx.guild.roles, name=self.ROLE_PRIZE)
            await ctx.author.add_roles(role_prize)
            await user_answer.delete()
            await ctx.author.remove_roles(role_quiz)

            secret_voice = discord.utils.get(ctx.guild.voice_channels, name=self.SECRET_VOICE)
            if voice_channel and secret_voice:
                await ctx.author.move_to(secret_voice)
                await self.play_greetings(secret_voice)
                await self.notify_prize_winners(ctx)

            await asyncio.sleep(60)
            await ctx.channel.set_permissions(ctx.guild.default_role, read_messages=True)

        except asyncio.TimeoutError:
            await ctx.author.remove_roles(role_quiz)
            await ctx.channel.set_permissions(ctx.guild.default_role, read_messages=True)

    
    class Settings(discord.ui.Modal):
        quiz_role = discord.ui.RoleSelect(
            custom_id='quiz_role',
            placeholder='Quiz role'
        )

        prize_role = discord.ui.RoleSelect(
            custom_id='prize_role',
            placeholder='Prize role'
        )

        quiz_channel = discord.ui.ChannelSelect(
            custom_id='quiz_channel',
            placeholder='Quiz channel'
        )

        secret_voice = discord.ui.ChannelSelect(
            custom_id='secret_voice',
            placeholder='Secret voice'
        )

        secret_text = discord.ui.ChannelSelect(
            custom_id='secret_text',
            placeholder='Secret text channel'
        )

        async def on_submit(self, interaction: discord.Interaction):
            await interaction.response.send_message(f'Thanks for your feedback, you chose:\n{self.quiz_role.name}\n{self.prize_role.name}\n{self.quiz_channel.name}\n{self.secret_voice.name}\n{self.secret_text.name}', ephemeral=True)

        async def on_error(self, interaction: discord.Interaction, error: Exception) -> None:
            await interaction.response.send_message('Oops! Something went wrong.', ephemeral=True)
            traceback.print_exception(type(error), error, error.__traceback__)