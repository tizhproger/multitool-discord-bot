import requests
import os
import json
import discord
from datetime import datetime, timedelta
from discord.ext import tasks

class Twitch():

    def __init__(self, clips_channel, twitch_channel, client_id, client_secret, twitch_token, refresh_token) -> None:
        self.clips_channel = clips_channel
        self.twitch_channel_name = twitch_channel
        self.client_id = client_id
        self.client_secret = client_secret
        self.token = twitch_token
        self.token_url = 'https://id.twitch.tv/oauth2/token'
        self.validate_url = 'https://id.twitch.tv/oauth2/validate'
        self.base_url = 'https://api.twitch.tv/helix'
        self.clips_url = 'https://api.twitch.tv/helix/clips'
        self.streams_url = 'https://api.twitch.tv/helix/streams'
        self.refresh_token = refresh_token
        self.token_expiration_time = None


    def refresh_twitch_access_token(self):
        # Parameters for refreshing the access token
        refresh_params = {
            'client_id': self.client_id,
            'client_secret': self.client_secret,
            'refresh_token': self.refresh_token,
            'grant_type': 'refresh_token',
        }

        # Make a POST request to refresh the access token
        response = requests.post(self.token_url, data=refresh_params)

        if response.status_code == 200:
            data = response.json()
            self.token = data['access_token']
            expires_in = data['expires_in']
            self.token_expiration_time = datetime.utcnow() + timedelta(seconds=expires_in)

            return self.token
        else:
            raise Exception(f"Failed to refresh access token. Response: {response.text}")


    def is_access_token_expired(self):
        if self.token_expiration_time is None:
            return True

        # Check if the access token has expired
        return datetime.utcnow() > self.token_expiration_time


    def validate_twitch_access_token(self):

        if self.is_access_token_expired():
            return False

        # Make a GET request to validate the access token
        headers = {'Authorization': f'Bearer {self.token}'}
        response = requests.get(self.validate_url, headers=headers)

        return response.status_code == 200

    # Function to get twitch authorization token
    def get_twitch_token(self):

        payload = {
            'client_id': self.client_id,
            'client_secret': self.client_secret,
            'grant_type': 'client_credentials'
        }

        response = requests.post('https://id.twitch.tv/oauth2/token', data=payload)

        if response.status_code == 200:
            data = response.json()
            self.token = data['access_token']
            expires_in = data['expires_in']
            self.token_expiration_time = datetime.utcnow() + timedelta(seconds=expires_in)
            return response.json()['access_token']
        else:
            return None


    def get_stream_info(self):
        stream_url = f'{self.streams_url}?user_login={self.twitch_channel_name}&type=live'
        headers = {'Client-ID': self.client_id, 'Authorization': f'Bearer {self.token}'}

        response = requests.get(stream_url, headers=headers)
        data = response.json()

        if 'data' in data and data['data']:
            return data['data'][0]['started_at'], data['data'][0]['user_id']
        else:
            return None

    # Function to get new clips from twitch channel
    def get_new_clips(self, starts, broadcaster_id):
        if self.token is None:
            self.get_twitch_token()

        if self.is_access_token_expired() or not self.validate_twitch_access_token():
            self.get_twitch_token()

        headers = {
            'Client-ID': self.client_id,
            'Authorization': f"Bearer {self.token}"
        }
        response = requests.get(f'{self.streams_url}?broadcaster_id={broadcaster_id}&&started_at={starts}', headers=headers)
        if response.status_code == 200:
            clips = response.json()['data']
            new_clips = False
            old_clips = False
            
            if os.path.exists("clips.json"):
                with open('clips.json', 'r') as file:
                    old_clips = json.load(file)
                    new_clips = [clip for clip in clips if clip['url'] not in old_clips]
            else:
                with open('clips.json', 'w') as file:
                    json.dump([clip['url'] for clip in clips], file, indent=2)
                    new_clips = [clip for clip in clips if clip['url']]

            if new_clips and old_clips:
                with open('clips.json', 'w') as file:
                    json.dump([clip['url'] for clip in clips], file, indent=2)
            
            return new_clips

    async def check_history(url, channel, time):
        async for message in channel.history(after=datetime.fromisoformat(time)):
            if url in message.content:
                return False
        return True

    # Function to check new clips on twitch channel every 5 seconds, if new clip was found - send to DS clips channel
    @tasks.loop(seconds = 5)
    async def check_for_clips(self, interaction: discord.Interaction):
        info = self.get_stream_info()
        if info is not None:
            stream_start_time = info[0]
            streamer_id = info[1]
            new_clips = self.get_new_clips(stream_start_time, streamer_id)
            if new_clips:
                for clip in new_clips:
                    clips_channel = discord.utils.get(interaction.guild.channels, name=self.clips_channel)
                    if not clips_channel:
                        print(f"Канал для клипов '{clips_channel}' не найден.")
                        return
                    else:
                        if await self.check_history(clip['url'], clips_channel, stream_start_time):
                            await clips_channel.send(f"{clip['title']} - {clip['url']}")
        else:
            print('Стример офлайн, перепроверка через 5 секунд...')

